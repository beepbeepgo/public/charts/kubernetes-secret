# Kubernetes Secrets Helm Chart

- ![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square)
- ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

This Helm chart allows the creation of any type of Kubernetes Secret including Opaque secrets, dockerconfigjson, and more.

## Prerequisites

- Helm 3.0+
- Kubernetes 1.14+

## Installation

To install the chart with the release name `my-release`:

```bash
helm install my-release ./path/to/kubernetes-secrets
```

Replace ./path/to/kubernetes-secrets with the path to the directory containing the chart.

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| dockerconfigjson | object | `{"password":"","registry":"","username":""}` | Hash of key, values which contain the required values to generate a the dockerconfigjson that will be base64 encoded and saved as a secret |
| dockerconfigjson.password | string | `""` | Docker registry password |
| dockerconfigjson.registry | string | `""` | Docker registry URL |
| dockerconfigjson.username | string | `""` | Docker registry username |
| dockerconfigjsonenc | string | `""` | base64-encoded docker configuration JSON |
| opaque | object | `{"password":"","username":""}` | A standard opaque secret |
| opaque.password | string | `""` | base64 encoded password |
| opaque.username | string | `""` | base64 encoded username |
| secretName | string | `"my-secret"` |  |
| secretType | string | `"Opaque"` |  |
