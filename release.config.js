const {
  semanticReleaseConfigDefault,
} = require("@beepbeepgo/semantic-release");
module.exports = semanticReleaseConfigDefault({
  plugins: {
    git: {
      appendAssets: ["chart/Chart.yaml"],
    },
  },
});
